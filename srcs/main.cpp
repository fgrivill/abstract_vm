#include "abstractvm.hpp"

int   main(int ac, char **av)
{
  if (ac > 1)
  {
    for (int i = 1; i < ac; i++)
      Parse(std::string(av[i]));
  }
  else
    Parse();
  return (0);
}
