#include "parse.hpp"

Parse::Parse(std::string f)
{
  _stack = new stack(false);
  _n = 0;
  on_line = false;

  file = new std::ifstream(f);
  try
  {
    if (!file->is_open())
      throw avm_exception("AbstractVM cannot read the file", __LINE__);
    else
    {
        while (++_n && std::getline(*file,line) && line != "exit")
        {
            clean_string(line);
            line = line.substr(0, line.find(";"));
            clean_string(line);
            if (!line.empty())
              _instructions.push_back(std::pair<std::string, int>(line, _n));
        }
        file->close();
        exec();
    }
  } catch (const std::exception & e)
  {
      std::cerr << "Error: " << e.what() << " " << f << std::endl;
  }
}

Parse::Parse(void)
{
  _stack = new stack(true);
  on_line = true;
  _n = 1;

  std::cout << "1:\t\033[36m->\033[0m ";
  while (std::getline(std::cin, line) && line != ";;")
  {
    clean_string(line);
    if (line == ";;")
      break ;
    line = line.substr(0, line.find(";"));
    clean_string(line);
    if (!line.empty())
      _instructions.push_back(std::pair<std::string, int>(line, _n));
      std::cout << ++_n << ":\t\033[36m->\033[0m ";
  }
  exec();
}

Parse::~Parse(void)
{
  return ;
}

Parse::Parse(Parse const &src)
{
  (void)src;
  return ;
}

Parse         &Parse::operator=(Parse const &rhs)
{
  *this = rhs;
  return (*this);
}

void          Parse::exec(void)
{
  bool    terminate = false;
  bool    error = false;

  for (std::vector<std::pair<std::string, int> >::iterator i = _instructions.begin(); i != _instructions.end(); i++)
  {
    try
    {
      _stack->select(i->first);
    }  catch (const std::exception &e)
    {
      error = true;
        std::cerr << "Error line " << i->second << ": " << e.what() << ": \"" << i->first << "\"" << std::endl;
        if (!DEBUG)
          break ;
    }
  }
  if (!error)
    terminate = true;
  try
  {
      if (terminate || DEBUG)
      _stack->exit(line);
  }   catch (const std::exception &e)
  {
    if (!on_line && !file->is_open())
    {
      delete file;
      return;
    }
    else
    {
        std::cerr << "Error line " << _n << ": " << e.what() << ": \"" << line << "\"" << std::endl;
        if (!on_line)
          delete file;
      }
  }
}

void          Parse::clean_string(std::string &line)
{
  size_t tmp;
  while ((tmp = line.find("\t")) != std::string::npos)
    line = line.replace(tmp, 1, " ");
  while ((tmp = line.find("  ")) != std::string::npos)
    line = line.replace(tmp, 2, " ");
  while (line[0] == ' ')
    line.erase(line.begin());
  while (line.size() && (line[line.size() - 1] == ' '))
    line.erase(line.size() - 1);
}
