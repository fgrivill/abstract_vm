#include "avm_exception.hpp"

avm_exception::avm_exception(const char *msg, int line)
{
    std::ostringstream oss;
    (void)line;
    oss << msg;
    this->msg = oss.str();
}

avm_exception::~avm_exception(void) throw() {  return ; }

const char * avm_exception::what() const throw()
{
    return (this->msg.c_str());
}

underflow::underflow() : avm_exception("underflow", 0) { return ; }

underflow::~underflow(void) throw() {  return ; }

overflow::overflow() : avm_exception("overflow", 0) { return ; }

overflow::~overflow(void) throw() {  return ; }
