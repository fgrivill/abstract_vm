#include "operand.hpp"

// std::map<eOperandType, std::type_info> Operand::convert = {{eOperandType::Int8, std::typeid(int8_t)},
// 																										{eOperandType::Int16, std::typeid(int16_t)},
// 																										{eOperandType::Int32, std::typeid(int32_t)},
// 																										{eOperandType::Float, std::typeid(float)},
// 																										{eOperandType::Double, std::typeid(double)}};

template <>
IOperand const *			Operand<int8_t>::operator%( IOperand const & rhs ) const {
	long double		ret;

	switch (rhs.getType()) {
		case 0:
			ret = mod(dynamic_cast<Operand<int8_t> const *>(&rhs));
			if (_type < rhs.getType()) {
				if (ret <= std::numeric_limits<int8_t>::max()) {
					if (ret >= std::numeric_limits<int8_t>::lowest())
						return new Operand<int8_t>(ret);
					else
						throw (avm_exception("Underflow", __LINE__));
				}
				else
					throw (avm_exception("Overflow", __LINE__));
			}
			break ;
		case 1:
			ret = mod(dynamic_cast<Operand<int16_t> const *>(&rhs));
			if (_type < rhs.getType()) {
					if (ret <= std::numeric_limits<int16_t>::max()) {
						if (ret >= std::numeric_limits<int16_t>::lowest())
							return new Operand<int16_t>(ret);
						else
							throw (avm_exception("Underflow", __LINE__));
					}
					else
						throw (avm_exception("Overflow", __LINE__));
			}
			break ;
		case 2:
			ret = mod(dynamic_cast<Operand<int32_t> const *>(&rhs));
			if (_type < rhs.getType()) {
					if (ret <= std::numeric_limits<int32_t>::max()) {
						if (ret >= std::numeric_limits<int32_t>::lowest())
							return new Operand<int32_t>(ret);
						else
							throw (avm_exception("Underflow", __LINE__));
					}
					else
						throw (avm_exception("Overflow", __LINE__));
			}
			break ;
		case 3:
			ret = 0;
			throw (avm_exception("Can not compute modulo on float or double", __LINE__));
			break;
		case 4:
			ret = 0;
			throw (avm_exception("Can not compute modulo on float or double", __LINE__));
			break ;
	}
	if (ret <= std::numeric_limits<int8_t>::max()) {
		if (ret >= std::numeric_limits<int8_t>::lowest())
			return new Operand<int8_t>(ret);
		else
			throw (avm_exception("Underflow", __LINE__));
	}
	else
		throw (avm_exception("Overflow", __LINE__));
};

template <>
IOperand const *			Operand<int16_t>::operator%( IOperand const & rhs ) const {
	long double		ret;

	switch (rhs.getType()) {
		case 0:
			ret = mod(dynamic_cast<Operand<int8_t> const *>(&rhs));
			if (_type < rhs.getType()) {
				if (ret <= std::numeric_limits<int8_t>::max()) {
					if (ret >= std::numeric_limits<int8_t>::lowest())
						return new Operand<int8_t>(ret);
					else
						throw (avm_exception("Underflow", __LINE__));
				}
				else
					throw (avm_exception("Overflow", __LINE__));
			}
			break ;
		case 1:
			ret = mod(dynamic_cast<Operand<int16_t> const *>(&rhs));
			if (_type < rhs.getType()) {
				if (ret <= std::numeric_limits<int16_t>::max()) {
					if (ret >= std::numeric_limits<int16_t>::lowest())
						return new Operand<int16_t>(ret);
					else
						throw (avm_exception("Underflow", __LINE__));
				}
				else
					throw (avm_exception("Overflow", __LINE__));
			}
			break ;
		case 2:
			ret = mod(dynamic_cast<Operand<int32_t> const *>(&rhs));
			if (_type < rhs.getType()) {
					if (ret <= std::numeric_limits<int32_t>::max()) {
						if (ret >= std::numeric_limits<int32_t>::lowest())
							return new Operand<int32_t>(ret);
						else
							throw (avm_exception("Underflow", __LINE__));
					}
					else
						throw (avm_exception("Overflow", __LINE__));
			}
			break ;
		case 3:
			ret = 0;
			throw (avm_exception("Can not compute modulo on float or double", __LINE__));
			break;
		case 4:
			ret = 0;
			throw (avm_exception("Can not compute modulo on float or double", __LINE__));
			break ;
	}
	if (ret <= std::numeric_limits<int16_t>::max() && ret >= std::numeric_limits<int16_t>::lowest())
		return new Operand<int16_t>(ret);
	if (ret <= std::numeric_limits<int16_t>::max()) {
		if (ret >= std::numeric_limits<int16_t>::lowest())
			return new Operand<int16_t>(ret);
		else
			throw (avm_exception("Underflow", __LINE__));
	}
	else
		throw (avm_exception("Overflow", __LINE__));
};

template <>
IOperand const *			Operand<int32_t>::operator%( IOperand const & rhs ) const {
	long double		ret;

	switch (rhs.getType()) {
		case 0:
			ret = mod(dynamic_cast<Operand<int8_t> const *>(&rhs));
			if (_type < rhs.getType()) {
				if (ret <= std::numeric_limits<int8_t>::max()) {
					if (ret >= std::numeric_limits<int8_t>::lowest())
						return new Operand<int8_t>(ret);
					else
						throw (avm_exception("Underflow", __LINE__));
				}
				else
					throw (avm_exception("Overflow", __LINE__));
			}
			break ;
		case 1:
			ret = mod(dynamic_cast<Operand<int16_t> const *>(&rhs));
			if (_type < rhs.getType()) {
					if (ret <= std::numeric_limits<int16_t>::max()) {
						if (ret >= std::numeric_limits<int16_t>::lowest())
							return new Operand<int16_t>(ret);
						else
							throw (avm_exception("Underflow", __LINE__));
					}
					else
						throw (avm_exception("Overflow", __LINE__));
			}
			break ;
		case 2:
			ret = mod(dynamic_cast<Operand<int32_t> const *>(&rhs));
			if (_type < rhs.getType()) {
				if (ret <= std::numeric_limits<int32_t>::max()) {
					if (ret >= std::numeric_limits<int32_t>::lowest())
						return new Operand<int32_t>(ret);
					else
						throw (avm_exception("Underflow", __LINE__));
				}
				else
					throw (avm_exception("Overflow", __LINE__));
			}
			break ;
		case 3:
			ret = 0;
			throw (avm_exception("Can not compute modulo on float or double", __LINE__));
			break;
		case 4:
			ret = 0;
			throw (avm_exception("Can not compute modulo on float or double", __LINE__));
			break ;
	}
	if (ret <= std::numeric_limits<int32_t>::max()) {
		if (ret >= std::numeric_limits<int32_t>::lowest())
			return new Operand<int32_t>(ret);
		else
			throw (avm_exception("Underflow", __LINE__));
	}
	else
		throw (avm_exception("Overflow", __LINE__));
};
