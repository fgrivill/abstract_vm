#include "stack.hpp"

stack::stack(bool on_line)
{
  _on_line = on_line;
  return ;
}

stack::~stack(void)
{
  for (std::vector<IOperand const *>::iterator i = _stack.begin(); i != _stack.end();)
  {
    delete (*i);
    _stack.erase(i);
    i++;
  }
}

stack::stack(stack const & src)
{
  _stack = src.getStack();
}

stack &stack::operator=(stack const & rhs)
{
  *this = rhs;
  return (*this);
}

std::vector<IOperand const *>  stack::getStack(void) const { return (_stack); }

void	stack::push(IOperand const *data)
{
  _stack.emplace(_stack.begin(), data);
}

void	stack::pop(void)
{
  if (!_stack.empty())
  {
    delete (_stack.front());
    _stack.erase(_stack.begin());
  }
  else
    throw avm_exception("Instruction pop on an empty stack", __LINE__);
}
void	stack::dump(void)
{
  static std::map<eOperandType, std::string> convert = {{eOperandType::Int8, "int8"},
                                                      {eOperandType::Int16, "int16"},
                                                      {eOperandType::Int32, "int32"},
                                                      {eOperandType::Float, "float"},
                                                      {eOperandType::Double, "double"}};
  for (std::vector<IOperand const *>::iterator i = _stack.begin(); i != _stack.end(); i++)
    std::cout << colors.find(convert.find((*i)->getType())->second)->second << (*i)->toString() << "\033[0m" << std::endl;
}

void	stack::assert(IOperand const *data)
{
  static std::map<eOperandType, std::string> convert = {{eOperandType::Int8, "int8"},
                                                      {eOperandType::Int16, "int16"},
                                                      {eOperandType::Int32, "int32"},
                                                      {eOperandType::Float, "float"},
                                                      {eOperandType::Double, "double"}};
    if (!_stack.size())
          throw avm_exception("Instruction assert on empty stack", __LINE__);
	if (data->getType() != _stack[0]->getType() || data->toString() != _stack[0]->toString())
	{
     std::string tmp = "An assert instruction is not true: [";
     tmp += colors.find(convert.find(data->getType())->second)->second + data->toString() + " \033[0m!= ";
     tmp += colors.find(convert.find(_stack[0]->getType())->second)->second + _stack[0]->toString() + "\033[0m]";
     throw avm_exception(tmp.c_str() , __LINE__);
    }
}

void	stack::add(void)
{
    if (_stack.size() < 2)
      throw avm_exception("The stack is composed of strictly less that two values when an arithmetic instruction is executed.", __LINE__);
    const IOperand  *tmp = (ORDER)? *(_stack[0]) + *(_stack[1]) : *(_stack[1]) + *(_stack[0]);
    pop();
    pop();
    push(tmp);
}

void	stack::sub(void)
{
    if (_stack.size() < 2)
        throw avm_exception("The stack is composed of strictly less that two values when an arithmetic instruction is executed.", __LINE__);
    const IOperand  *tmp = (ORDER)? *(_stack[0]) - *(_stack[1]) : *(_stack[1]) - *(_stack[0]);
    pop();
    pop();
    push(tmp);
}
void	stack::mul(void)
{
  if (_stack.size() < 2)
    throw avm_exception("The stack is composed of strictly less that two values when an arithmetic instruction is executed.", __LINE__);
  const IOperand  *tmp = (ORDER)? *(_stack[0]) * *(_stack[1]) : *(_stack[1]) * *(_stack[0]);
  pop();
  pop();
  push(tmp);
}

void	stack::div(void)
{
    if (_stack.size() < 2)
        throw avm_exception("The stack is composed of strictly less that two values when an arithmetic instruction is executed.", __LINE__);
    const IOperand  *tmp = (ORDER)? *(_stack[0]) / *(_stack[1]) : *(_stack[1]) / *(_stack[0]);
    pop();
    pop();
    push(tmp);
}

void	stack::mod(void)
{
    if (_stack.size() < 2)
        throw avm_exception("The stack is composed of strictly less that two values when an arithmetic instruction is executed.", __LINE__);
    const IOperand  *tmp = (ORDER)? *(_stack[0]) % *(_stack[1]) : *(_stack[1]) % *(_stack[0]);
    pop();
    pop();
    push(tmp);
}

void	stack::print(void)
{
  IOperand const *tmp = new Operand<int8_t>(static_cast<int8_t>(std::stod(_stack[0]->toString())));
  try
  {
    assert(tmp);
    int tmp2 = std::stoi(_stack[0]->toString());
    std::cout << static_cast<char>(tmp2);
    delete tmp;
  } catch (const std::exception & e)
  {
    delete tmp;
    std::string tmp3 = e.what();
    tmp3 = tmp3.substr(0, tmp3.find("\n"));
    throw avm_exception(tmp3.c_str(), __LINE__);
  }
}

void  stack::exit(std::string line)
{
  if ((!_on_line && line != "exit") || (_on_line && line != ";;"))
    throw avm_exception("The program doesn’t have an exit instruction", __LINE__);
}

bool  stack::is_correct_entry(std::string line, size_t it)
{
  bool    point = false;
  bool    first_number = false;

  while(it <= line.size())
  {
    while (!first_number && line[it] == ' ')
      it++;
    if ((!first_number && line[it] == '-') || (line[it] >= '0' && line[it] <= '9'))
    {
      if (!first_number)
        first_number = true;
    }
    else if ((line[it] == '.' || line[it] == ',') && !point)
      point = true;
    else if (line[it] == ' ' || (line[it] == ')' && !line[it + 1]))
    {
      while (it <= line.size() && line[it] == ' ')
        it++;
      if (line[it] == ')' && !line[it + 1])
        return true;
      return false;
    }
    else
      return false;
    it++;
  }
  return false;
}

std::map<std::string, fn1> stack::one = {{"push", &stack::push},
                                    {"assert", &stack::assert}};

std::map<std::string, fn2> stack::two = {{"pop", &stack::pop},
                                          {"dump", &stack::dump},
                                          {"add", &stack::add},
                                          {"sub", &stack::sub},
                                          {"mul", &stack::mul},
                                          {"div", &stack::div},
                                          {"mod", &stack::mod},
                                          {"print", &stack::print},
                                          {"square", &stack::square},
                                          {"help", &stack::help}};

std::map<std::string, eOperandType> stack::type = {{"int8", Int8},
                                          {"int16", Int16},
                                          {"int32", Int32},
                                          {"float", Float},
                                          {"double", Double}};
std::map<std::string, std::string> stack::colors = {{"int8", "\033[31m"},
                                                    {"int16", "\033[32m"},
                                                    {"int32", "\033[33m"},
                                                    {"float", "\033[34m"},
                                                    {"double", "\033[35m"}};

IOperand const *               stack::createOperand(eOperandType type, std::string const & value) const
{
    switch (type)
    {
        case Int8: return (createInt8(value));
        case Int16: return (createInt16(value));
        case Int32: return (createInt32(value));
        case Float: return (createFloat(value));
        case Double: return (createDouble(value));
    }
}

IOperand const * stack::createInt8( std::string const & value ) const
{
    int8_t tmp;

    int tmp1 = std::stoi(value);
    if (tmp1 < -128 || tmp1 > 127)
        throw avm_exception("It's not a valiable value", __LINE__);
    tmp = static_cast<int8_t>(tmp1);
    return (new Operand<int8_t>(tmp));
}
IOperand const * stack::createInt16( std::string const & value ) const
{
    int16_t tmp;
    int tmp2 = std::stoi(value);
    if (tmp2 < -32768 || tmp2 > 32767)
        throw avm_exception("It's not a valiable value", __LINE__);
    tmp = static_cast<int16_t>(tmp2);
    return (new Operand<int16_t>(tmp));
}
IOperand const * stack::createInt32( std::string const & value ) const
{
    try
    {
        int32_t tmp;
        int tmp3 = std::stoi(value);
        tmp = static_cast<int32_t>(tmp3);
        return (new Operand<int32_t>(tmp));
    } catch (const std::exception & e)
    {
        throw avm_exception("It's not a valiable value", __LINE__);
    }
}
IOperand const * stack::createFloat( std::string const & value ) const
{
    float tmp3;

    try
    {
        tmp3 = std::stof(value);
    } catch (const std::exception & e)
    {
        throw avm_exception("It's not a valiable value", __LINE__);
    }
    return (new Operand<float>(tmp3));
}
IOperand const * stack::createDouble( std::string const & value ) const
{
    double tmp4;

    try
    {
        tmp4 = std::stod(value);
    } catch (const std::exception & e)
    {
        throw avm_exception("It's not a valiable value", __LINE__);
    }
    return (new Operand<double>(tmp4));
}


void stack::select(std::string line)
{
  std::map<std::string, fn1>::iterator tmp1 = one.find(line.substr(0, line.find(" ")));
  if (tmp1 != one.end())
  {
    std::string   type_str = line.substr(line.find(" ") + 1, line.find("(") - (line.find(" ") + 1));
    std::map<std::string, eOperandType>::iterator tmp_type = type.find(type_str);
    if (tmp_type != type.end())
    {
      if (is_correct_entry(line, line.find("(") + 1))
      {
        std::string tmp_to_cast = line.substr(line.find("(") + 1, line.find(")") - line.find("(") - 1);
        while (tmp_to_cast[0] == ' ')
          tmp_to_cast.erase(tmp_to_cast.begin());
        while (tmp_to_cast.size() && (tmp_to_cast[tmp_to_cast.size() - 1] == ' '))
          tmp_to_cast.erase(tmp_to_cast.size() - 1);
        CALL_MEMBER_FN(*this , tmp1->second)(createOperand(tmp_type->second, tmp_to_cast));
      }
      else
        throw avm_exception("An instruction is unknown", __LINE__);
    }
    else
      throw avm_exception("An instruction is unknown", __LINE__);
  }
  else if (two.find(line) != two.end())
    CALL_MEMBER_FN(*this , two.find(line)->second)();
  else if (line == "exit")
    return ;
  else
    throw avm_exception("An instruction is unknown", __LINE__);
}


/*
** BONUS
*/

void	stack::square(void)
{
  if (!_stack.size())
    throw avm_exception("The stack is composed of strictly less that one values when an arithmetic instruction is executed.", __LINE__);
    const IOperand  *tmp = *(_stack[0]) * *(_stack[0]);
    pop();
    push(tmp);
}

void stack::help(void)
{
  if (!_on_line)
    return ;
  std::cout << "S := INSTR [SEP INSTR]* #" << std::endl << std::endl << "INSTR :=" << std::endl;
  for (std::map<std::string, fn1>::iterator i = one.begin(); i != one.end(); i++)
  {
    std::cout << "\t";
    if (i != one.begin())
      std::cout <<  "|";
    std::cout << "\t\033[32m" << i->first << "\033[0m VALUE" << std::endl;
  }
  for (std::map<std::string, fn2>::iterator i = two.begin(); i != two.end(); i++)
    std::cout << "\t|\t\033[32m" << i->first << "\033[0m" << std::endl;
  std::cout << std::endl << "VALUE :=" << std::endl;
  for (std::map<std::string, eOperandType>::iterator i = type.begin(); i != type.end(); i++)
  {
    std::cout << "\t";
    if (i != type.begin())
      std::cout <<  "|";
    std::cout << "\t" << colors.find(i->first)->second << i->first << "\033[0m" << "(N)" << std::endl;
  }
  std::cout << std::endl << "SEP := '\\n'+" << std::endl;
}
