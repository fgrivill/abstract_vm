#ifndef STACK_HPP
# define STACK_HPP

# include <vector>
# include <string>
# include <iostream>
# include <map>
# include <functional>

# include "operand.hpp"
# include "avm_exception.hpp"

# define ORDER 0

class stack;

typedef  void (stack::*fn1)(IOperand const *);
typedef  void (stack::*fn2)(void);
# define CALL_MEMBER_FN(object,ptrToMember)  ((object).*(ptrToMember))

class stack
{
  public:
    stack(bool on_line);
    virtual ~stack(void);
    stack(stack const & src);

    stack                   &operator=(stack const & rhs);
    std::vector<IOperand const *>  getStack(void) const;
    IOperand const *               createOperand(eOperandType type, std::string const & value) const;

    void  select(std::string line);
    void	push(IOperand const *data);
  	void	pop(void);
  	void	assert(IOperand const *data);
  	void	dump(void);
  	void	add(void);
  	void	sub(void);
  	void	mul(void);
  	void	div(void);
  	void	mod(void);
  	void	print(void);
    void    exit(std::string line);

    /*
    ** BONUS
    */
    void	square(void);
    void    help(void);

    /*
    ** END BONUS
    */
  private:
    std::vector<IOperand const *>    _stack;
    bool                              _on_line;
    static std::map<std::string, fn1> one;
    static std::map<std::string, fn2> two;
    static std::map<std::string, eOperandType>  type;
    static std::map<std::string, std::string> colors;
    bool   is_correct_entry(std::string line, size_t it);
    IOperand const * createInt8( std::string const & value ) const;
    IOperand const * createInt16( std::string const & value ) const;
    IOperand const * createInt32( std::string const & value ) const;
    IOperand const * createFloat( std::string const & value ) const;
    IOperand const * createDouble( std::string const & value ) const;
    std::vector<std::string>::iterator      find_in_vector(std::vector<std::string> v, std::string to_find);
};

# endif
