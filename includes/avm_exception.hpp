#ifndef AVM_EXCEPTION_HPP
# define AVM_EXCEPTION_HPP

# include <exception>
# include <string>
# include <sstream>

class avm_exception : public std::exception
{
  public:
      avm_exception(const char *msg, int line);
      virtual ~avm_exception() throw();
      const char * what() const throw();
  private:
      std::string msg;
};

class underflow : public avm_exception
{
  public:
    underflow();
    virtual ~underflow() throw();
};

class overflow : public avm_exception
{
  public:
    overflow();
    virtual ~overflow() throw();
};

#endif
