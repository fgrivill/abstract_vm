#ifndef OPERAND_HPP
# define OPERAND_HPP

# include "ioperand.hpp"
# include "avm_exception.hpp"
# include <limits>
# include <type_traits>

# include <iostream>
# include <map>
# include <typeindex>

template <typename T>
class Operand : public IOperand {
private:
	int					_precision;
	eOperandType		_type;
	T					_value;
	std::string			_str;
	// static std::map<eOperandType, std::type_info> convert;
	Operand(void);
	eOperandType		detectType(void) {
		if (std::is_same<T, int8_t>::value)
			return eOperandType::Int8;
		else if (std::is_same<T, int16_t>::value)
			return eOperandType::Int16;
		else if (std::is_same<T, int32_t>::value)
			return eOperandType::Int32;
		else if (std::is_same<T, float>::value)
			return eOperandType::Float;
		else if (std::is_same<T, double>::value)
			return eOperandType::Double;
		throw (avm_exception("Operand Type unsupported", __LINE__));
	}

	template <typename AD>
	long double			add( Operand<AD> const * rhs ) const {
		return (static_cast<long double>(_value) + static_cast<long double>(rhs->getValue()));
	}

	template <typename SU>
	long double			sub( Operand<SU> const * rhs ) const {
		return (static_cast<long double>(_value) - static_cast<long double>(rhs->getValue()));
	}

	template <typename MU>
	long double			mul( Operand<MU> const * rhs ) const {
		return (static_cast<long double>(_value) * static_cast<long double>(rhs->getValue()));
	}

	template <typename DI>
	long double			div( Operand<DI> const * rhs ) const {
		long double		div;

		div = static_cast<long double>(rhs->getValue());
		if (div == 0) {
			throw avm_exception("Division by zero", __LINE__);
		}
		return (static_cast<long double>(_value) / div);
	}

	template <typename MO>
	int32_t				mod( Operand<MO> const * rhs ) const {
		int32_t			mod;

		mod = static_cast<int32_t>(rhs->getValue());
		if (mod == 0) {
			throw avm_exception("Modulo by zero", __LINE__);
		}
		return (static_cast<int32_t>(_value) % mod);
	}

public:
	int					getPrecision( void ) const { return _precision; };
	eOperandType 		getType( void ) const { return _type; };
	T					getValue( void ) const { return _value; };


// 		if (_type < rhs.getType())
// 	IOperand const *	operator+( IOperand const & rhs ) const
// 	{
//  		long double		ret;
// 		size_t	type = convert.find(rhs.getType())->second.hash_code();
//
// 		ret = add(dynamic_cast<Operand<type> const *>(&rhs));
// 		{
// 			if (ret <= std::numeric_limits<type>::max())
// 			{
// 				if (ret >= std::numeric_limits<type>::lowest())
// 					return new Operand<type>(ret);
// 				else
// 					throw (underflow());
// 			}
// 			else
// 				throw (overflow());
// 		}
// 		if (ret <= std::numeric_limits<T>::max())
// 		{
// 			if (ret >= std::numeric_limits<T>::lowest())
// 				return new Operand<T>(ret);
// 			else
// 				throw (underflow());
// 		}
// 		else
// 			throw (overflow());
// }

	IOperand const *	operator+( IOperand const & rhs ) const {
		long double		ret;

		switch (rhs.getType()) {
			case 0:
				ret = add(dynamic_cast<Operand<int8_t> const *>(&rhs));
				if (_type < rhs.getType()) {
					if (ret <= std::numeric_limits<int8_t>::max()) {
						if (ret >= std::numeric_limits<int8_t>::lowest())
							return new Operand<int8_t>(ret);
						else
							throw (underflow());
					}
					else
						throw (overflow());
				}
				break ;
			case 1:
				ret = add(dynamic_cast<Operand<int16_t> const *>(&rhs));
				if (_type < rhs.getType()) {
					if (ret <= std::numeric_limits<int16_t>::max()) {
						if (ret >= std::numeric_limits<int16_t>::lowest())
							return new Operand<int16_t>(ret);
						else
								throw (underflow());
					}
					else
						throw (overflow());
				}
				break ;
			case 2:
				ret = add(dynamic_cast<Operand<int32_t> const *>(&rhs));
				if (_type < rhs.getType()) {
					if (ret <= std::numeric_limits<int32_t>::max()) {
						if (ret >= std::numeric_limits<int32_t>::lowest())
							return new Operand<int32_t>(ret);
						else
							throw (underflow());
					}
					else
						throw (overflow());
				}
				break ;
			case 3:
				ret = add(dynamic_cast<Operand<float> const *>(&rhs));
				if (_type < rhs.getType()) {
					if (ret <= std::numeric_limits<float>::max()) {
						if (ret >= std::numeric_limits<float>::lowest())
							return new Operand<float>(ret);
						else
							throw (underflow());
					}
					else
						throw (overflow());
				}
				break;
			case 4:
				ret = add(dynamic_cast<Operand<double> const *>(&rhs));
				if (_type < rhs.getType()) {
					if (ret <= std::numeric_limits<double>::max()) {
						if (ret >= std::numeric_limits<double>::lowest())
							return new Operand<double>(ret);
						else
							throw (underflow());
					}
					else
						throw (overflow());
				}
				break ;
		}
		if (ret <= std::numeric_limits<T>::max()) {
			if (ret >= std::numeric_limits<T>::lowest())
				return new Operand<T>(ret);
			else
				throw (underflow());
		}
		else
			throw (overflow());
	};

	IOperand const *	operator-( IOperand const & rhs ) const {
		long double		ret;

		switch (rhs.getType()) {
			case 0:
				ret = sub(dynamic_cast<Operand<int8_t> const *>(&rhs));
				if (_type < rhs.getType()) {
					if (ret <= std::numeric_limits<int8_t>::max()) {
						if (ret >= std::numeric_limits<int8_t>::lowest())
							return new Operand<int8_t>(ret);
						else
							throw (underflow());
					}
					else
						throw (overflow());
				}
				break ;
			case 1:
				ret = sub(dynamic_cast<Operand<int16_t> const *>(&rhs));
				if (_type < rhs.getType()) {
					if (ret <= std::numeric_limits<int16_t>::max()) {
						if (ret >= std::numeric_limits<int16_t>::lowest())
							return new Operand<int16_t>(ret);
						else
							throw (underflow());
					}
					else
						throw (overflow());
				}
				break ;
			case 2:
				ret = sub(dynamic_cast<Operand<int32_t> const *>(&rhs));
				if (_type < rhs.getType()) {
					if (ret <= std::numeric_limits<int32_t>::max()) {
						if (ret >= std::numeric_limits<int32_t>::lowest())
							return new Operand<int32_t>(ret);
						else
							throw (underflow());
					}
					else
						throw (overflow());
				}
				break ;
			case 3:
				ret = sub(dynamic_cast<Operand<float> const *>(&rhs));
				if (_type < rhs.getType()) {
					if (ret <= std::numeric_limits<float>::max()) {
						if (ret >= std::numeric_limits<float>::lowest())
							return new Operand<float>(ret);
						else
							throw (underflow());
					}
					else
						throw (overflow());
				}
				break;
			case 4:
				ret = sub(dynamic_cast<Operand<double> const *>(&rhs));
				if (_type < rhs.getType()) {
					if (ret <= std::numeric_limits<double>::max()) {
						if (ret >= std::numeric_limits<double>::lowest())
							return new Operand<double>(ret);
						else
							throw (underflow());
					}
					else
						throw (overflow());
				}
				break ;
		}
		if (ret <= std::numeric_limits<T>::max()) {
			if (ret >= std::numeric_limits<T>::lowest())
				return new Operand<T>(ret);
			else
				throw (underflow());
		}
		else
			throw (overflow());
	};

	IOperand const *	operator*( IOperand const & rhs ) const {
		long double		ret;

		switch (rhs.getType()) {
			case 0:
				ret = mul(dynamic_cast<Operand<int8_t> const *>(&rhs));
				if (_type < rhs.getType()) {
					if (ret <= std::numeric_limits<int8_t>::max()) {
						if (ret >= std::numeric_limits<int8_t>::lowest())
							return new Operand<int8_t>(ret);
						else
							throw (underflow());
					}
					else
						throw (overflow());
				}
				break ;
			case 1:
				ret = mul(dynamic_cast<Operand<int16_t> const *>(&rhs));
				if (_type < rhs.getType()) {
					if (ret <= std::numeric_limits<int16_t>::max()) {
						if (ret >= std::numeric_limits<int16_t>::lowest())
							return new Operand<int16_t>(ret);
						else
							throw (underflow());
					}
					else
						throw (overflow());
				}
				break ;
			case 2:
				ret = mul(dynamic_cast<Operand<int32_t> const *>(&rhs));
				if (_type < rhs.getType()) {
					if (ret <= std::numeric_limits<int32_t>::max()) {
						if (ret >= std::numeric_limits<int32_t>::lowest())
							return new Operand<int32_t>(ret);
						else
							throw (underflow());
					}
					else
						throw (overflow());
				}
				break ;
			case 3:
				ret = mul(dynamic_cast<Operand<float> const *>(&rhs));
				if (_type < rhs.getType()) {
					if (ret <= std::numeric_limits<float>::max()) {
						if (ret >= std::numeric_limits<float>::lowest())
							return new Operand<float>(ret);
						else
							throw (underflow());
					}
					else
						throw (overflow());
				}
				break;
			case 4:
				ret = mul(dynamic_cast<Operand<double> const *>(&rhs));
				if (_type < rhs.getType()) {
					if (ret <= std::numeric_limits<double>::max()) {
						if (ret >= std::numeric_limits<double>::lowest())
							return new Operand<double>(ret);
						else
							throw (underflow());
					}
					else
						throw (overflow());
				}
				break ;
		}
		if (ret <= std::numeric_limits<T>::max()) {
			if (ret >= std::numeric_limits<T>::lowest())
				return new Operand<T>(ret);
			else
				throw (underflow());
		}
		else
			throw (overflow());
	};

	IOperand const *	operator/( IOperand const & rhs ) const {
		long double		ret;

		switch (rhs.getType()) {
			case 0:
				ret = div(dynamic_cast<Operand<int8_t> const *>(&rhs));
				if (_type < rhs.getType()) {
					if (ret <= std::numeric_limits<int8_t>::max()) {
						if (ret >= std::numeric_limits<int8_t>::lowest())
							return new Operand<int8_t>(ret);
						else
							throw (underflow());
					}
					else
						throw (overflow());
				}
				break ;
			case 1:
				ret = div(dynamic_cast<Operand<int16_t> const *>(&rhs));
				if (_type < rhs.getType()) {
					if (ret <= std::numeric_limits<int16_t>::max()) {
						if (ret >= std::numeric_limits<int16_t>::lowest())
							return new Operand<int16_t>(ret);
						else
							throw (underflow());
					}
					else
						throw (overflow());
				}
				break ;
			case 2:
				ret = div(dynamic_cast<Operand<int32_t> const *>(&rhs));
				if (_type < rhs.getType()) {
					if (ret <= std::numeric_limits<int32_t>::max()) {
						if (ret >= std::numeric_limits<int32_t>::lowest())
							return new Operand<int32_t>(ret);
						else
							throw (underflow());
					}
					else
						throw (overflow());
				}
				break ;
			case 3:
				ret = div(dynamic_cast<Operand<float> const *>(&rhs));
				if (_type < rhs.getType()) {
					if (ret <= std::numeric_limits<float>::max()) {
						if (ret >= std::numeric_limits<float>::lowest())
							return new Operand<float>(ret);
						else
							throw (underflow());
					}
					else
						throw (overflow());
				}
				break;
			case 4:
				ret = div(dynamic_cast<Operand<double> const *>(&rhs));
				if (_type < rhs.getType()) {
					if (ret <= std::numeric_limits<double>::max()) {
						if (ret >= std::numeric_limits<double>::lowest())
							return new Operand<double>(ret);
						else
							throw (underflow());
					}
					else
						throw (overflow());
				}
				break ;
		}
		if (ret <= std::numeric_limits<T>::max()) {
			if (ret >= std::numeric_limits<T>::lowest())
				return new Operand<T>(ret);
			else
				throw (underflow());
		}
		else
			throw (overflow());
	};

	IOperand const *	operator%( IOperand const & rhs ) const {
		Operand<double> const	*tmp;

		tmp = dynamic_cast<Operand<double> const *>(&rhs);
		throw (avm_exception("Can not compute modulo on float or double", __LINE__));
		return tmp;
	};

	Operand<T> &		operator=( IOperand const & rhs ) {
		Operand<T> const	*tmp;

		tmp = dynamic_cast<Operand<T> const *>(&rhs);
		_value = tmp->getValue();
		return *this;
	}

	std::string const &	toString( void ) const {
		return _str;
	};

	Operand(T value) {
		_value = value;
		_type = detectType();
		_precision = sizeof(value);
		_str = std::to_string(_value);
	}
	virtual ~Operand( void ) {}
};

#endif
