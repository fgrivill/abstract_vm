#ifndef PARSE_HPP
# define PARSE_HPP

# include <fstream>

# include "stack.hpp"

# define DEBUG 0

class Parse
{
	public:
		Parse(std::string file);
		Parse(void);
		virtual ~Parse(void);
		Parse(Parse const &src);

		Parse         &operator=(Parse const &rhs);
		void          exec(void);
	private:
		stack         *_stack;
		void          clean_string(std::string &line);
		std::string                       line;
		double        _n;
		std::ifstream *file;
		bool          on_line;
		std::vector<std::pair<std::string, int> >      _instructions;
};

#endif
