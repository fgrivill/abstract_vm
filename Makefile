NAME = AbstractVM

CLASS_FILES = operand avm_exception parse stack

FILES_SRCS = $(CLASS_FILES) main
FOLDER_SRCS = ./srcs/
SRCS = $(addprefix $(FOLDER_SRCS), $(addsuffix .cpp, $(FILES_SRCS)))

OBJS = $(SRCS:.cpp=.o)

FILES_INCS = ioperand abstractvm $(CLASS_FILES)
FOLDER_INCS = ./includes/
INCLUDES = $(addprefix $(FOLDER_INCS), $(addsuffix .hpp, $(FILES_INCS)))
INCS = -I$(FOLDER_INCS)

CC = clang++
CFLAGS = -Wall -Wextra -Werror -std=c++11

RM = /bin/rm -f

all: $(NAME)

$(NAME): $(OBJS)
	$(CC) -o $(NAME) $(OBJS)

%.o: %.cpp $(INCLUDES)
	$(CC) $(CFLAGS) $(INCS) -o $@ -c $<

clean:
	$(RM) $(OBJS)

fclean: clean
	$(RM) $(NAME)

re: fclean all

.PHONY: clean fclean re
